from api.serializers import ReviewProductSerializer, ReviewProductUpdateOnlyContentSerializer
from api.models import ReviewProduct
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import get_object_or_404
import requests
import json

def verify_token(token):
     url = 'http://tk.oauth.getoboru.xyz/token/resource'
     response = requests.get(
          url,
          headers={'Authorization' : '{}'.format(token)},
          data={}
     )
     if response.status_code == 200:
          return True     
     return False

def send_log(type, message):
     url = 'http://tk.logger.getoboru.xyz/{}'.format(type)
     data = {'message': message, 'app': 'Review Product Service'}
     headers = {'Content-type': 'application/json'}
     r = requests.post(url, data=json.dumps(data), headers=headers)

class ReviewProductView(APIView):
     def post(self, request):
          if not 'HTTP_AUTHORIZATION' in request.META:
               send_log('error', 'Unauthorized user is accessing endpoint post review product with no token')
               return Response(
                    {
                         'error': 'No token provided!'
                    },
                    status=status.HTTP_401_UNAUTHORIZED
               )

          is_token_verified = verify_token(request.META['HTTP_AUTHORIZATION'])
          if not is_token_verified:
               send_log('error', 'Unauthorized user is accessing endpoint post review product with invalid token')
               return Response(
                    {
                         'error': 'Invalid token provided!'
                    },
                    status=status.HTTP_401_UNAUTHORIZED
               )
          serializer = ReviewProductSerializer(data=request.data)
          if serializer.is_valid():
               serializer.save()
               send_log('info', 'Authorized user is accessing endpoint create a new review product')
               return Response(
                    {
                         'data' : serializer.data
                    },
                    status = status.HTTP_201_CREATED
               )
          else:
               send_log('error', 'Authorized user has failed to create a new review product')
               return Response(
                    {
                         'error' : serializer.errors
                    },
                    status=status.HTTP_400_BAD_REQUEST
               )
     
     def get(self, request, pk=None):
          if pk:
               product_review = get_object_or_404(ReviewProduct, pk=pk)
               serializer = ReviewProductSerializer(product_review)
               send_log('info', 'User is accessing endpoint get a certain review product by id review')
               return Response(
                    {
                         'data': serializer.data
                    },
                    status=status.HTTP_200_OK
               )
          product_reviews = ReviewProduct.objects.all()
          serializer = ReviewProductSerializer(product_reviews, many=True)
          send_log('info', 'User is accessing endpoint get all review products')
          return Response(
               {
                    'data': serializer.data
               },
               status=status.HTTP_200_OK
          )
     
     def put(self, request, pk=None):
          if not 'HTTP_AUTHORIZATION' in request.META:
               send_log('error', 'Unauthorized user is accessing endpoint update review product with no token')
               return Response(
                    {
                         'error': 'No token provided!'
                    },
                    status=status.HTTP_401_UNAUTHORIZED
               )

          is_token_verified = verify_token(request.META['HTTP_AUTHORIZATION'])
          if not is_token_verified:
               send_log('error', 'Unauthorized user is accessing endpoint update review product with invalid token')
               return Response(
                    {
                         'error': 'Invalid token provided!'
                    },
                    status=status.HTTP_401_UNAUTHORIZED
               )
          review_product = get_object_or_404(ReviewProduct, pk=pk)
          serializer_update = ReviewProductUpdateOnlyContentSerializer(review_product, data=request.data)
          if serializer_update.is_valid():
               serializer_update.save()
               updated_review_product = get_object_or_404(ReviewProduct, pk=pk)
               serializer = ReviewProductSerializer(updated_review_product)
               send_log('info', 'Authorized user is accessing endpoint update a review product')
               return Response(
                    {
                         'data': serializer.data
                    },
                    status=status.HTTP_200_OK
               )
          else:
               send_log('error', 'Authorized user has failed to update a review product')
               return Response(
                    {
                         'error' : serializer.errors
                    },
                    status=status.HTTP_400_BAD_REQUEST
               )
     def delete(self, request, pk=None):
          if not 'HTTP_AUTHORIZATION' in request.META:
               send_log('error', 'Unauthorized user is accessing endpoint delete review product with no token')
               return Response(
                    {
                         'error': 'No token provided!'
                    },
                    status=status.HTTP_401_UNAUTHORIZED
               )

          is_token_verified = verify_token(request.META['HTTP_AUTHORIZATION'])
          if not is_token_verified:
               send_log('error', 'Unauthorized user is accessing endpoint delete review product with invalid token')
               return Response(
                    {
                         'error': 'Invalid token provided!'
                    },
                    status=status.HTTP_401_UNAUTHORIZED
               )
          product_review = get_object_or_404(ReviewProduct, pk=pk)
          product_review.delete()
          send_log('info', 'Authorized user is accessing endpoint delete a review product')
          return Response(
               {"data": "Review has been Deleted"}
          )

class ReviewProductByIdProduct(APIView):
     def get(self, request, idproduct):
          list_review_product = ReviewProduct.objects.filter(product_id=idproduct)
          serializers = ReviewProductSerializer(list_review_product, many=True)
          send_log('info', 'User is accessing endpoint get review product by id product')
          return Response(
               {
                    'data': serializers.data
               },
               status=status.HTTP_200_OK
          )
