from django.contrib import admin
from api.models import ReviewProduct

class ReviewProductAdmin(admin.ModelAdmin):
     list_display = ('id', 'product_id', 'reviewer_username', 'content', 'created_at', 'updated_at')

admin.site.register(ReviewProduct, ReviewProductAdmin)