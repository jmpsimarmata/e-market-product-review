from django.urls import path
from api.views import ReviewProductView, ReviewProductByIdProduct

app_name = 'api'
urlpatterns = [
     path('review-product/', ReviewProductView.as_view(), name='reviewProductView'),
     path('review-product/<int:pk>', ReviewProductView.as_view(), name='reviewProductViewWithId'),
     path('reviews-by-id-product/<str:idproduct>', ReviewProductByIdProduct.as_view(), name='reviewByIdProductView')
]
