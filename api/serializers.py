from dataclasses import fields
from rest_framework import serializers
from api.models import ReviewProduct

class ReviewProductSerializer(serializers.ModelSerializer):
     class Meta:
          model = ReviewProduct
          fields = '__all__'

class ReviewProductUpdateOnlyContentSerializer(serializers.ModelSerializer):
     class Meta:
          model = ReviewProduct
          fields = ('content', )