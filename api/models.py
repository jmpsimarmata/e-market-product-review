from django.db import models

class ReviewProduct(models.Model):
     product_id = models.CharField(max_length=255)
     reviewer_username = models.CharField(max_length=255)
     reviewer_image = models.CharField(max_length=255, default="https://icon-library.com/images/no-user-image-icon/no-user-image-icon-27.jpg")
     content = models.TextField(max_length=255)
     created_at = models.DateTimeField(auto_now_add=True)
     updated_at = models.DateTimeField(auto_now=True)

     def __str__(self) -> str:
         return '{} - {}: {}'.format(self.product_id, self.reviewer_username, self.content)